import pytest


@pytest.mark.asyncio
async def test_basic(requests):
    response = await requests.post(
        "http://127.0.0.1:5826",
        params={"ref": "init.test"},
        json={"ref": "init.test", "args": [1, 2, 3, 4], "kwargs": {"test": 1}},
    )
    data = await response.json()
    assert response.status == 200, data
    assert data["kwargs"]["test"] == 1
    assert data["args"] == [1, 2, 3, 4], data


@pytest.mark.asyncio
async def test_fail(requests):
    response = await requests.post(
        "http://127.0.0.1:5826", json={"ref": "nonexistent.ref"}
    )
    assert response.status != 200
    data = await response.json()
    assert "error" in data


@pytest.mark.asyncio
async def test_no_ref(requests):
    response = await requests.post("http://127.0.0.1:5826", json={})
    assert response.status != 200
    data = await response.json()
    assert "error" in data


@pytest.mark.asyncio
async def test_nested(requests):
    payload = {"ref": "init.test", "kwargs": {"nested": {"nest": {"nest": {}}}}}
    response = await requests.post("http://127.0.0.1:5826", json=payload)
    data = await response.json()
    assert response.status == 200, data
    assert data["kwargs"]["nested"] == {"nest": {"nest": {}}}
